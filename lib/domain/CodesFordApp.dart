import 'package:codesford_app/pages/StartPage.dart';
import 'package:flutter/material.dart';

class CodesFordApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'CodesFord',
      theme: ThemeData(
          fontFamily: 'Roboto',
          textTheme: TextTheme(
            headline1: TextStyle(
              fontSize: 36.0,
              fontFamily: 'Roboto',
              color: Colors.black,
              fontWeight: FontWeight.normal,
            ),
            headline2: TextStyle(
              fontSize: 24.0,
              fontFamily: 'Roboto',
              fontWeight: FontWeight.bold,
              color: Colors.white,
            ),
            headline3: TextStyle(
              fontSize: 24.0,
              fontFamily: 'Roboto',
              color: Colors.black,
              fontWeight: FontWeight.normal,
            ),
            headline4: TextStyle(
              fontSize: 16.0,
              fontFamily: 'Roboto',
              fontWeight: FontWeight.w300,
              color: Colors.white,
            ),
            headline5: TextStyle(
              fontSize: 16.0,
              fontFamily: 'Roboto',
              fontWeight: FontWeight.w300,
              color: Colors.black,
            ),
            headline6: TextStyle(
                fontSize: 16.0,
                fontFamily: 'Roboto',
                fontWeight: FontWeight.w300,
                color: Color.fromRGBO(152, 145, 145, 1)),
            bodyText1: TextStyle(
                fontSize: 14.0, fontFamily: 'Roboto', color: Colors.black),
          ),
          primaryColor: Colors.white,
          accentColor: Color.fromRGBO(135, 154, 255, 0.8)),
      home: StartPage(),
    );
  }
}

import 'package:codesford_app/domain/CodesFordApp.dart';
import 'package:flutter/material.dart';

main() => runApp(CodesFordApp());

import 'package:flutter/material.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: new AppBar(
          title: new Text('CodesFord'),
        ),
        body: Padding(
            padding: EdgeInsets.only(top: 20, bottom: 100, left: 20, right: 20),
            child: Column(
              children: <Widget>[
                _logoLittle(context, 'Categories:'),
                new Container(height: 250, child: _categoriesCards(context)),
                SizedBox(
                  height: 20,
                ),
                _logoLittle(context, 'Recommendations:'),
              ],
            )),
        bottomNavigationBar: BottomNavigationBar(
          items: const <BottomNavigationBarItem>[
            BottomNavigationBarItem(
              icon: Icon(Icons.home),
              label: 'Home',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.alarm_on_sharp),
              label: 'ToDo',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.search),
              label: 'Search',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.person),
              label: 'Profile',
            ),
          ],
          currentIndex: 1,
          selectedItemColor: Color.fromRGBO(58, 81, 212, 1),
          unselectedItemColor: Theme.of(context).textTheme.headline6.color,
          onTap: (ind) {},
        ));
  }

  Widget _categoriesCards(BuildContext context) {
    return GridView.count(
        crossAxisCount: 2,
        padding: EdgeInsets.all(4.0),
        childAspectRatio: 8.0 / 5.0,
        children: <Widget>[
          Card(
              color: Color.fromRGBO(240, 146, 119, 1),
              child: new Center(
                child: new Text(
                  'HTML5',
                  style: Theme.of(context).textTheme.headline5,
                ),
              )),
          Card(
              color: Color.fromRGBO(99, 199, 227, 1),
              child: new Center(
                child: new Text(
                  'JavaScript',
                  style: Theme.of(context).textTheme.headline5,
                ),
              )),
          Card(
              color: Color.fromRGBO(129, 229, 176, 1),
              child: new Center(
                child: new Text(
                  'CSS',
                  style: Theme.of(context).textTheme.headline5,
                ),
              )),
          Card(
              color: Color.fromRGBO(251, 180, 117, 1),
              child: new Center(
                child: new Text(
                  'Bootstrap5',
                  style: Theme.of(context).textTheme.headline5,
                ),
              )),
        ]);
  }

  Widget _logoLittle(BuildContext context, String text) {
    return Align(
      alignment: Alignment
          .centerLeft, // Align however you like (i.e .centerRight, centerLeft)
      child: Padding(
        padding: EdgeInsets.only(left: 8),
        child: Text(
          text,
          style: Theme.of(context).textTheme.headline3,
        ),
      ),
    );
  }
}

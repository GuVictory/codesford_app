import 'package:codesford_app/pages/AuthorizationPage.dart';
import 'package:codesford_app/pages/HomePage.dart';
import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';

class PaymentAskPage extends StatefulWidget {
  @override
  _PaymentAskPageState createState() => _PaymentAskPageState();
}

class _PaymentAskPageState extends State<PaymentAskPage> {
  Widget _button(String text, void func()) {
    return RaisedButton(
        splashColor: Theme.of(context).accentColor,
        highlightColor: Color.fromRGBO(34, 60, 204, 1),
        color: Color.fromRGBO(34, 60, 204, 0.7),
        child: Text(text, style: Theme.of(context).textTheme.headline4),
        onPressed: () {
          func();
        },
        shape:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(5.0)));
  }

  @override
  Widget build(BuildContext context) {
    return Stack(children: <Widget>[
      Image.asset(
        'assets/images/pay_bg.png',
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        fit: BoxFit.cover,
      ),
      Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Text(
              'Buy a subscription for 1\$ and get:',
              style: Theme.of(context).textTheme.headline2,
            ),
            SizedBox(
              height: 20,
            ),
            Text(
              '- access to a useful newsletter',
              style: Theme.of(context).textTheme.headline2,
            ),
            Text(
              '- advice on the training program',
              style: Theme.of(context).textTheme.headline2,
            ),
            Text(
              '- employment tips',
              style: Theme.of(context).textTheme.headline2,
            ),
            Padding(
                padding: EdgeInsets.only(top: 100),
                child: Column(
                  children: <Widget>[
                    _button('Buy a subscription', () {}),
                    Padding(
                      padding: EdgeInsets.all(10),
                      child: GestureDetector(
                        child: Text(
                          'Skip...',
                          style: Theme.of(context).textTheme.headline4,
                        ),
                        onTap: () {
                          Navigator.push(
                              context,
                              PageTransition(
                                  type: PageTransitionType.rightToLeft,
                                  child: HomePage()));
                        },
                      ),
                    ),
                  ],
                ))
          ])
    ]);
  }
}

import 'package:codesford_app/pages/PaymentAskPage.dart';
import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';

class AuthorizationPage extends StatefulWidget {
  @override
  _AuthorizationPageState createState() => _AuthorizationPageState();
}

class _AuthorizationPageState extends State<AuthorizationPage> {
  TextEditingController _emailEditingController = TextEditingController();
  TextEditingController _passwordEditingController = TextEditingController();
  TextEditingController _passwordRepeatEditingController =
      TextEditingController();

  String _email;
  String _passwod;
  String _passwodRepeat;
  bool _showLogin = true;

  Widget _logo(String label) {
    return Padding(
      padding: EdgeInsets.only(top: 150),
      child: Container(
        child: Align(
          child: Text(
            label,
            style: Theme.of(context).textTheme.headline1,
          ),
        ),
      ),
    );
  }

  Widget _input(Icon icon, String hint,
      TextEditingController textEditingController, bool obsecure) {
    return Container(
      padding: EdgeInsets.only(left: 20, right: 20),
      child: TextField(
        controller: textEditingController,
        obscureText: obsecure,
        style: Theme.of(context).textTheme.headline5,
        decoration: InputDecoration(
            filled: true,
            fillColor: Colors.white,
            hintStyle: Theme.of(context).textTheme.headline6,
            hintText: hint,
            focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(5),
                borderSide: BorderSide(
                  color: Color.fromRGBO(34, 60, 204, 1),
                  width: 3,
                )),
            enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(5),
                borderSide: BorderSide(
                  color: Theme.of(context).accentColor,
                  width: 1,
                )),
            prefixIcon: Padding(
              padding: EdgeInsets.only(left: 10, right: 10),
              child: IconTheme(
                data: IconThemeData(
                  color: Theme.of(context).accentColor,
                ),
                child: icon,
              ),
            )),
      ),
    );
  }

  Widget _button(String text, void func()) {
    return RaisedButton(
        splashColor: Theme.of(context).accentColor,
        highlightColor: Color.fromRGBO(34, 60, 204, 1),
        color: Theme.of(context).primaryColor,
        child: Text(text, style: Theme.of(context).textTheme.headline5),
        onPressed: () {
          func();
        },
        shape:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(5.0)));
  }

  Widget _buttonPrimary(String text, void func()) {
    return RaisedButton(
        splashColor: Theme.of(context).accentColor,
        highlightColor: Color.fromRGBO(34, 60, 204, 1),
        color: Color.fromRGBO(34, 60, 204, 0.5),
        child: Text(text, style: Theme.of(context).textTheme.headline4),
        onPressed: () {
          func();
        },
        shape:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(5.0)));
  }

  Widget _form(String label, void func(), String changeBtnText) {
    return Container(
      decoration: BoxDecoration(
        color: Theme.of(context).accentColor,
        borderRadius: BorderRadius.all(const Radius.circular(13)),
      ),
      margin: const EdgeInsets.only(top: 100, left: 20, right: 20),
      child: Padding(
        padding: EdgeInsets.all(10),
        child: Column(
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(bottom: 20, top: 30),
              child: _input(
                  Icon(Icons.email), 'Email', _emailEditingController, false),
            ),
            Padding(
              padding: EdgeInsets.only(bottom: 20, top: 10),
              child: _input(Icon(Icons.lock), 'Password',
                  _passwordEditingController, true),
            ),
            (!_showLogin
                ? Padding(
                    padding: EdgeInsets.only(bottom: 20, top: 10),
                    child: _input(Icon(Icons.lock), 'Repeat Password',
                        _passwordRepeatEditingController, true),
                  )
                : SizedBox(
                    height: 20,
                  )),
            Padding(
              padding: EdgeInsets.only(left: 20, right: 20),
              child: Container(
                height: 50,
                width: MediaQuery.of(context).size.width,
                child: _buttonPrimary(
                    label, _showLogin ? _loginUser : _registerUser),
              ),
            ),
            Padding(
              padding:
                  EdgeInsets.only(left: 20, right: 20, top: 10, bottom: 20),
              child: Container(
                height: 50,
                width: MediaQuery.of(context).size.width,
                child: _button(changeBtnText, () {
                  setState(() {
                    _showLogin = !_showLogin;
                  });
                }),
              ),
            )
          ],
        ),
      ),
    );
  }

  void _loginUser() {
    _email = _emailEditingController.text;
    _passwod = _passwordEditingController.text;

    _emailEditingController.clear();
    _passwordEditingController.clear();

    new Future.delayed(
        const Duration(seconds: 1),
        () => Navigator.push(
            context,
            PageTransition(
                type: PageTransitionType.rightToLeft,
                child: PaymentAskPage())));
  }

  void _registerUser() {
    _email = _emailEditingController.text;
    _passwod = _passwordEditingController.text;
    _passwodRepeat = _passwordRepeatEditingController.text;

    _emailEditingController.clear();
    _passwordEditingController.clear();
    _passwordRepeatEditingController.clear();

    new Future.delayed(
        const Duration(seconds: 1),
        () => Navigator.push(
            context,
            PageTransition(
                type: PageTransitionType.rightToLeft,
                child: PaymentAskPage())));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).primaryColor,
      body: Column(
        children: <Widget>[
          (_showLogin ? _logo('Login') : _logo('Register')),
          (_showLogin
              ? _form('Login', () {}, 'Register')
              : _form('Register', () {}, 'Login'))
        ],
      ),
    );
  }
}
